package com.app.eventpoptest.ui.eventsdetail

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EventsDetailViewModelFactory(
        private val context: Context,
        private val viewController: EventsDetailFragment?,
        private val position: Int
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return EventsDetailViewModel(context,viewController,position) as T
    }
}
