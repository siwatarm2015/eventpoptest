package com.app.eventpoptest.ui.eventsdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.eventpoptest.R

class EventsDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.events_detail_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, EventsDetailFragment.newInstance())
                .commitNow()
        }
    }

}
