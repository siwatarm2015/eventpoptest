package com.app.eventpoptest.ui.eventsdetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.eventpoptest.R
import com.app.eventpoptest.databinding.ItemTicketBinding
import com.app.eventpoptest.models.TicketType

class EventsDetailAdapter(
    private val ticket: ArrayList<TicketType>
) : RecyclerView.Adapter<EventsDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemTicketBinding = DataBindingUtil.inflate(inflater, R.layout.item_ticket, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = ticket.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    inner class ViewHolder(private val binding: ItemTicketBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.position = position
            binding.model = ticket[position]
            binding.adapter = this
            binding.executePendingBindings()
        }
    }
}


