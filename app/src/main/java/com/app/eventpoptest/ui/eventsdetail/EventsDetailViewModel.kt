package com.app.eventpoptest.ui.eventsdetail

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.app.eventpoptest.R
import com.app.eventpoptest.extension.toTextShow
import com.app.eventpoptest.models.EventDetail
import com.app.eventpoptest.utils.JsonUtil
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class EventsDetailViewModel(
        private val context: Context,
        private val viewController: EventsDetailFragment?,
        private var position: Int
) : ViewModel() {

    private lateinit var eventDetail : EventDetail
    val eventTitle : ObservableField<String> = ObservableField("")
    val eventDate : ObservableField<String> = ObservableField("")
    val location : ObservableField<String> = ObservableField("")
    val eventDesc : ObservableField<String> = ObservableField("")

    init {
        getRawJson()
    }

    private fun getRawJson() {

        when(position){
            0 -> context.resources.openRawResource(R.raw.event_1).let { JsonUtil.getJsonFile(it) }.apply { fetchToModel(this) }
            1 -> context.resources.openRawResource(R.raw.event_2).let { JsonUtil.getJsonFile(it) }.apply { fetchToModel(this) }
            2 -> context.resources.openRawResource(R.raw.event_3).let { JsonUtil.getJsonFile(it) }.apply { fetchToModel(this) }
            3 -> context.resources.openRawResource(R.raw.event_4).let { JsonUtil.getJsonFile(it) }.apply { fetchToModel(this) }
            4 -> context.resources.openRawResource(R.raw.event_5).let { JsonUtil.getJsonFile(it) }.apply { fetchToModel(this) }
            else -> viewController?.onEventNotFound()
        }
    }

    private fun fetchToModel(jsonString: String?) {
        val listType = object : TypeToken<EventDetail>() {}.type
        eventDetail =  Gson().fromJson<EventDetail>(jsonString,listType)
        updateView()
        viewController?.initTicketList(eventDetail.event.ticket_types)
    }

    private fun updateView() {
        val date = "${eventDetail.event.start_at.toTextShow()} - ${eventDetail.event.end_at.toTextShow()}"
        eventTitle.set(eventDetail.event.title)
        eventDate.set(date)
        location.set(eventDetail.event.location)
        eventDesc.set(eventDetail.event.description)
        viewController?.loadEventImage("http://www.${eventDetail.event.cover}")
    }
}
