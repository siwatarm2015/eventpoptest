package com.app.eventpoptest.ui.events

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.eventpoptest.R
import com.app.eventpoptest.databinding.EventsFragmentBinding
import com.app.eventpoptest.models.Events
import com.app.eventpoptest.ui.eventsdetail.EventsDetailActivity


class EventsFragment : Fragment(),EventsAdapter.OnEventItemClickedListener {

    lateinit var binding: EventsFragmentBinding
    lateinit var adapter: EventsAdapter

    companion object {
        fun newInstance() = EventsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.events_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewController = this

        binding.recyclerViewEvent.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.recyclerViewEvent.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = ViewModelProviders.of(this,
            context?.let { EventsViewModelFactory(it,binding.viewController) }).get(EventsViewModel::class.java)
    }

    fun initEventList(events: ArrayList<Events>) {
        adapter = EventsAdapter(events,context!!)
        adapter.onEventItemClickedListener = this
        binding.recyclerViewEvent.adapter = adapter
    }

    override fun onEventItemClick(position: Int) {
        Intent(activity,EventsDetailActivity::class.java).apply {
            putExtra("position",position)
            startActivity(this)
        }
    }


}
