package com.app.eventpoptest.ui.events

import android.content.Context
import androidx.lifecycle.ViewModel
import com.app.eventpoptest.R
import com.app.eventpoptest.models.Events
import com.app.eventpoptest.utils.JsonUtil.Companion.getJsonFile
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class EventsViewModel(
    private val context: Context,
    private val viewController: EventsFragment?
) : ViewModel() {

    init {
        getRawJson()
    }

    private fun getRawJson() {
        val myJson = context.resources.openRawResource(R.raw.events).let { getJsonFile(it) }
        val listType = object : TypeToken<List<Events>>() {}.type
        val eventsList =  Gson().fromJson<ArrayList<Events>>(myJson,listType)
        viewController?.initEventList(eventsList)
    }

}
