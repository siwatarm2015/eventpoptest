package com.app.eventpoptest.ui.events

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class EventsViewModelFactory(
    private val context: Context,
    private val viewController: EventsFragment?
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return EventsViewModel(context,viewController) as T
    }
}
