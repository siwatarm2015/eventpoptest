package com.app.eventpoptest.ui.events

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.eventpoptest.R
import com.app.eventpoptest.ui.events.EventsFragment

class EventsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.events_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, EventsFragment.newInstance())
                .commitNow()
        }
    }

}
