package com.app.eventpoptest.ui.events

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.eventpoptest.R
import com.app.eventpoptest.databinding.ItemEventBinding
import com.app.eventpoptest.extension.toTextShow
import com.app.eventpoptest.models.Events
import com.bumptech.glide.Glide

class EventsAdapter(
    private val events: ArrayList<Events>,
    private val context: Context
) : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    var onEventItemClickedListener: OnEventItemClickedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemEventBinding = DataBindingUtil.inflate(inflater, R.layout.item_event, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    inner class ViewHolder(private val binding: ItemEventBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.position = position
            binding.model = events[position]
            binding.startDate = events[position].start_at.toTextShow()
            binding.endDate = events[position].end_at.toTextShow()
            binding.adapter = this
            binding.executePendingBindings()

            Glide.with(context).load("http://www.${events[position].cover}").into(binding.imageView)
        }

        fun eventItemClick(position: Int){
            onEventItemClickedListener?.onEventItemClick(position)
        }

    }

    interface OnEventItemClickedListener {
        fun onEventItemClick(position: Int)
    }

}


