package com.app.eventpoptest.ui.eventsdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.eventpoptest.R
import com.app.eventpoptest.databinding.EventsDetailFragmentBinding
import com.app.eventpoptest.models.TicketType
import com.bumptech.glide.Glide

class EventsDetailFragment : Fragment() {

    lateinit var binding: EventsDetailFragmentBinding
    lateinit var adapter: EventsDetailAdapter
    private var position = 0

    companion object {
        fun newInstance() = EventsDetailFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.events_detail_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewController = this

        binding.recyclerView.isFocusable = false
        binding.recyclerView.isNestedScrollingEnabled = false
        binding.recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.intent?.extras?.let {
            position = it.getInt("position",0)
        }

        binding.viewModel = ViewModelProviders.of(this,
            context?.let { EventsDetailViewModelFactory(it, binding.viewController,position) }).get(EventsDetailViewModel::class.java)
    }

    fun initTicketList(ticket: ArrayList<TicketType>) {
        adapter = EventsDetailAdapter(ticket)
        binding.recyclerView.adapter = adapter
    }

    fun onEventNotFound(){
        binding.emptyView.showError()
    }

    fun onExitClick() {
        activity?.finish()
    }

    fun loadEventImage(cover: String) {
        context?.let { Glide.with(it).load(cover).into(binding.eventImage) }
    }

}
