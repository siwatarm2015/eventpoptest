package com.app.eventpoptest.extension

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val SHOW_DATE_FORMAT = "dd MMM yyyy"

@SuppressLint("SimpleDateFormat")
fun String?.toTextShow(): String {
    return if (!this.isNullOrEmpty()) {
        val findFormat = this.searchFormat()
        val simpleDateFormat = SimpleDateFormat(findFormat, Locale.ENGLISH)
        val newSimpleDateFormat = SimpleDateFormat(SHOW_DATE_FORMAT)
        val date = simpleDateFormat.parse(this)
        newSimpleDateFormat.format(date)
    } else {
        ""
    }
}

@SuppressLint("SimpleDateFormat")
fun String?.searchFormat(): String {
    val formats = arrayOf(
        "yyyy-MM-dd'T'HH:mm:ss'Z'",
        "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss",
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
        "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss",
        "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'", "MM/dd/yyyy'T'HH:mm:ss.SSSZ",
        "MM/dd/yyyy'T'HH:mm:ss.SSS", "MM/dd/yyyy'T'HH:mm:ssZ",
        "MM/dd/yyyy'T'HH:mm:ss", "yyyy:MM:dd HH:mm:ss",
        "yyyyMMdd", "yyyy MM dd'T'HH:mm:ss'Z'",
        "yyyy MM dd'T'HH:mm:ssZ", "yyyy MM dd'T'HH:mm:ss",
        "yyyy MM dd'T'HH:mm:ss.SSS'Z'",
        "yyyy MM dd'T'HH:mm:ss.SSSZ", "yyyy MM dd HH:mm:ss",
        "dd-MM-yyyy'T'HH:mm:ss'Z'", "dd-MM-yyyy'T'HH:mm:ssZ",
        "dd-MM-yyyy'T'HH:mm:ss", "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'",
        "dd-MM-yyyy'T'HH:mm:ss.SSSZ", "dd-MM-yyyy HH:mm:ss",
        "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy'T'HH:mm:ss.SSS'Z'",
        "dd/MM/yyyy'T'HH:mm:ss.SSSZ", "dd/MM/yyyy'T'HH:mm:ss.SSS",
        "dd/MM/yyyy'T'HH:mm:ssZ", "dd/MM/yyyy'T'HH:mm:ss",
        "dd:MM:yyyy HH:mm:ss", "ddMMyyyy",
        "dd MM yyyy'T'HH:mm:ss'Z'", "dd MM yyyy'T'HH:mm:ssZ",
        "dd MM yyyy'T'HH:mm:ss", "dd MM yyyy'T'HH:mm:ss.SSS'Z'",
        "dd MM yyyy'T'HH:mm:ss.SSSZ", "dd MM yyyy HH:mm:ss",
        "EEE MMM dd yyyy HH:mm:ss GMT'Z' (UTC)")

    if (this != null) {
        for (parse in formats) {
            try {
                val sdf = SimpleDateFormat(parse, Locale.ENGLISH)
                sdf.parse(this)
                return parse
            } catch (e: ParseException) {
            } catch (r: IllegalArgumentException) {
            }
        }
    } else {
        return "Not found format"
    }
    return this
}