package com.app.eventpoptest.models

data class TicketType(val id: Int,
                      val name: String,
                      val price: Double)