package com.app.eventpoptest.models

data class Events(val id: Int, val title: String, val start_at: String, val end_at: String, val location: String, val cover: String)
