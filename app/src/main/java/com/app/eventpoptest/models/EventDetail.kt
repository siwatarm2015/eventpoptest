package com.app.eventpoptest.models

data class EventDetail(val event: EventData){

    data class EventData(val id: Int,
                         val title: String,
                         val start_at: String,
                         val end_at: String,
                         val location: String,
                         val description: String,
                         val cover: String,
                         val ticket_types: ArrayList<TicketType>)
}