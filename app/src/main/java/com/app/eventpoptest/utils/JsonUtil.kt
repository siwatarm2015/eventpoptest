package com.app.eventpoptest.utils

import java.io.InputStream

class JsonUtil {

    companion object {
        fun getJsonFile(inputStream: InputStream): String? {
            return try {
                val bytes = ByteArray(inputStream.available())
                inputStream.read(bytes, 0, bytes.size)
                String(bytes)
            } catch (e: Throwable) {
                null
            }
        }

    }

}