package com.app.eventpoptest.ui.events


import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.app.eventpoptest.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class EventsDetailNotFoundTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(EventsActivity::class.java)

    @Test
    fun eventsActivityTest2() {

        onView(withId(R.id.recyclerViewEvent)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(6, click()))

        Thread.sleep(3000)

        onView(withId(R.id.imageButton)).perform(click())

    }
}
